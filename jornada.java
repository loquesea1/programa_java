//RAÚL_HERNÁNDEZ_LÓPEZ
//freeenergy@gmail.com
//16 de octubre del 2020

//Escriba un algoritmo que, dado el número de horas trabajadas por un empleado
//y el sueldo por hora, calcule el sueldo total de ese
//empleado. Tenga en cuenta que las horas extras se pagan el doble.

import java.util.Scanner;
public class jornada{
   public static void main(String args[]){
   Scanner entrada = new Scanner(System.in);
   //Declaracion de variables
   Double Extra, Jornada_Normal, Jornada_Extra, Hora_Pago;
   int Hora_Entrada, Horas_Extra, Hora_Salida, Total_Horas;
   //Recopila datos
   System.out.print ("\nPago por hora $");
   Hora_Pago = entrada.nextDouble();
   System.out.print ("\nHora de entrada (Formato de 24 horas):");
   Hora_Entrada = entrada.nextInt();
   System.out.print ("\nHora salida (Formato de 24 horas):");
   Hora_Salida = entrada.nextInt();
   Total_Horas = Hora_Salida - Hora_Entrada;
   //Calcula el monto de un jornada normal
	if(Total_Horas < 9 ){
           Jornada_Normal = Hora_Pago * Total_Horas;
           System.out.print("Monto a cobrar [$" + Jornada_Normal +  "]");
        }
	//Calcula el monto de una jornada con horas extras
	else if (Total_Horas > 8){
	   Horas_Extra = Total_Horas - 8;
           Extra = Hora_Pago * 2;
           Jornada_Extra = (Horas_Extra * Extra) + ( 8 * Hora_Pago );
	   System.out.print ("Monto a pagar [" + Jornada_Extra + "]");
	}
   }
} 
