/* Autor: Hernandez Lopez Raul @Neo
 * Correo: freeenergy1975@gmail.com
 * Fecha: dieciseis de Julio del 21
 * Tema: Algoritmo DDA*/

import java.util.Scanner;

class AlgoritmoDDA{
     public void DDA(int x1, int y1, int x2, int y2){
	   int x, dx, dy, steps;
           float incx, incy, xi, yi;  

           dx = x2 - x1;
           dy = y2 - y1;
           steps = Math.max(Math.abs(dx), Math.abs(dy));

           xi = x1;
           yi = y1;
           incx = dx/steps;
           incy = dy/steps;

           for(x = 0; x <= steps; x++){
               System.out.print("\n x :" + Math.round(xi) + " y: " + Math.round(yi));
               xi += incx;
               yi += incy;
           }
           System.out.print("\n");
       }

    public static void main (String Neo[]){
       AlgoritmoDDA obj = new AlgoritmoDDA();	    
       Scanner entrada = new Scanner(System.in);	    	    
       String coordenadasimp[] = {"x1", "y1", "x2", "y2"};
       int coordenadas[] = new int[4];
       int k;
       //Obtiene las cordenadas iniciales y finales.
       for(k = 0; k<= 3; k++ ){
           System.out.print( coordenadasimp[k] + " :");
	   coordenadas[k] = entrada.nextInt();
       }
       obj.DDA(coordenadas[0], coordenadas[1], coordenadas[2], coordenadas[3]);
    }
}
