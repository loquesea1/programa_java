//Raul hernandez lopez(Berserker)
//freeenergy1975@gmail.com
//Jueves 3 de septiembre del 2020

//Este programa permite almacenar un numero N de ventas y separarla de acuerdo a una serie de
//condiciones: ventas mayores a 10,000 pero menores de 20,000 Ventas iguales o menores
//de 10,000... cuanto es el monto de cada una y el monto global 

import java.util.Scanner;
//inicio de la clase
public class Ventas{
	//inicio del metodo main
	public static void main(String args[]){
	
		Scanner entrada = new Scanner(System.in);
		int numero, x, posicion, mayor = 0, menor = 0;
		int sumaMayor= 0, sumaMenor = 0, montoGlobal = 0;
		System.out.print("\nIngresa tu numero de ventas :");
		numero = entrada.nextInt();
		int ventas[] = new int[numero];
		//Inicio del ciclo	
		for(x = 0; x < numero; x++ ){
			posicion = x + 1;
			System.out.print("\nIngresa el valor de la venta " + posicion + " :");
			ventas[x] = entrada.nextInt();
		
			//En esta variable se almacena la suma total de todas las vantas
			montoGlobal = montoGlobal + ventas[x];
			//antes de que el ciclo continue tiene que pasar por un filtro 
			//compuesto de dos.
			//separa las ventas menores a diez min
			if((ventas[x] >= 0) && (ventas[x]<= 10000)){
				sumaMenor = sumaMenor + ventas[x];
				menor += 1;
			}
			//separa las ventas mayores a diez mil
			else if((ventas[x] > 10000) && (ventas[x] <= 20000)){

				sumaMayor = sumaMayor + ventas[x];
				mayor = mayor + 1;
			}
	 	}//Fin del ciclo for

	//impresion de resultados
	System.out.print("\nRealizaste " + menor + " ventas menores a diez mil y la
 	   + "suma de ellas es de :$ " + sumaMenor);
	System.out.print("\nRealizaste " + mayor + " ventas mayores a diez mil y la suma de estas es de :$ " + sumaMayor);
	System.out.print("\nMientras que el monto global es de :$" + montoGlobal + "\n");
	}//fin del metodo main
}//fin de la clase ventas


