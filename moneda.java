//Raul_Hernandez_Lopez
//freeenergy1975@gmail.com
//05 de octubre del 2020

//Realiza la conversión de pesos a dolares o euros 


import java.util.Scanner;
//inicio de la clase moneda
public class moneda{
	//inicio del metodo main
	public static void main(String args[]){
	Scanner entrada = new Scanner(System.in);
	//Declaración de variables
	double dolares = 22.09, euros = 21.91, conversion, pesos, new_mon;
	int x, y = 0, continuar = 0 ;	
	
	do{
	System.out.print("\nIngresa la cantidad de pesos que tienes : ");
	pesos = entrada.nextFloat();
	System.out.print("\nA qué tipo de moneda quieres hacer la conversion ? :");
	System.out.print("\n1) Dolares\n2) Euros\n3) otro");
	System.out.println("\nElija su opcion : ");
	x = entrada.nextInt();
	String Nombre_moneda[] = new String [10];	
	//variable que almacena la posición 
	y = y +1;
	switch (x){
		case 1://Realiza la conversion a dolares
		   conversion = pesos / dolares;
		   System.out.println("El número de pesos equivale a [" + conversion + "] dolares");
		break;
		case 2://Realiza la conversion a euros
		   conversion = pesos / euros;
		   System.out.println("El número de pesos equivale a [" + conversion + "] euros");
		break;
		case 3://Realiza la conversion a una moneda n
	           System.out.println("cuál es el nombre de la moneda?");
		   Nombre_moneda[y] = entrada.next();
		   System.out.println("A cuánto pesos Méxicanos equivale");
		   new_mon = entrada.nextDouble();
		   conversion = pesos / new_mon;
		   System.out.println("La cantidad de pesos equivale a [" + conversion + "] " + Nombre_moneda[y]);
		break;
		default://Despliega un mensaje si es que no se selecciona una opcion valida
		   System.out.println("Por favor ingresa una opcion valida");
		   continuar = entrada.nextInt();
		}
	//Despliega la opcion continuar o salir.
	System.out.println("Que deseas hacer\n1)vover\n0)salir"); 
	continuar = entrada.nextInt();
	} while(continuar == 1);
	
	}//fin del metodo
}//fin de la clase moneda
