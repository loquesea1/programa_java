//Raul   Hernandez   Lopez
//freeenergy1975@gmail.com
//16  de  octubre del 2020

/*Description:
*Calcular el descuento y el monto a pagar por un medicamento cualquiera 
*en una farmacia si todos los medicamentos tienen un descuento del 35%.*/

import java.text.DecimalFormat;
import java.util.Scanner;

//Inicio clase Farmacia
public class Farmacia{	
    //Inicio metodo main
    public static void main(String args[]){

	//Declaracion de variables.
        Scanner entrada = new Scanner(System.in);
	DecimalFormat limite = new DecimalFormat("#.##");
	int x = 0;
        double precio_Descuento, precio_Medicamento, descuento;
        String precio, eleccion;
	String nombre[] = new String[10];

	do{
	       x++;
	       //Recopila datos.
               System.out.print("\nNombre del medicamento :");
               nombre[x] = entrada.next();

               System.out.print("\nPrecio real del medicamento $");
               precio = entrada.next();
	    /*try iniciando con un parseo de datos, en caso de que surja un error se enviara 
	     *al  catch,  en  este  caso  el  que  se  podria  presentar  es  conversion. */
	    try{	
	       precio_Medicamento = Float.parseFloat(precio);
	   
	       precio_Descuento = precio_Medicamento * 0.65;
	       descuento = precio_Medicamento - precio_Descuento;

	       //Imprime resultados
	       System.out.print("\nDescuento\tMonto a pagar\n$" + limite.format(descuento) 
	       + "\t\t$" + limite.format(precio_Descuento) + "\n"
	       + "_________________________________");

	       System.out.print("\nDeseas continuar yes/no :");
	       eleccion = entrada.next();
	    }//Fin try
	    //Validacion de datos numericos
	    catch(NumberFormatException exception){
	    	System.out.print("\nAlguno de los valores introducidos\n"
				+ "No son validos, por favor vuelve a\n"
				+ "Comenzar.\n");
		eleccion = "yes";
	    }//Fin catch
	}while(eleccion.equalsIgnoreCase("yes"));
    }//Fin medo main.
}//Fin clase Farmacia.
