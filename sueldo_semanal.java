//Raul_Hernandez_LopePz
//freeenergy1975@gmail.com
//10 de octubre del 2020

//Determina el sueldo semanal de N trabajadores considerando que se
//les descuenta 5% de su sueldo si ganan entre 0 y 150 pesos.
//Se les descuenta 7% si ganan más de 150 pero menos de 300
//y 9% si ganan más de 300 pero menos de 450.
//Los datos son horas trabajadas, sueldo por hora y nombre de cada trabajador

import java.util.Scanner;
//inicio de la clase sueldo_semanal
public class sueldo_semanal{
	//Inicio clase main
	public static void main(String args[]){
		Scanner entrada = new Scanner(System.in);
		//Declaracion de variables
		double Horas_Trabajadas, Pago_Horas, Sueldo_Semanal, Sueldo_Final, Horas_Pago;
		int Dias_Trabajados, Numero_Trabajadores, x;
		System.out.print("Número de trabajadores :");
		Numero_Trabajadores = entrada.nextInt();
		//Declaracion del arreglo que contendra el nombre de los trabajadores asi como
		//el numero de nombres que contendra.
		String Nombres[] = new String[Numero_Trabajadores] ;
		
		for(x = 0; x <= Numero_Trabajadores - 1; x++){
		   //Recopila datos
		   System.out.print("\nNombre :");
		   Nombres[x] = entrada.next();
		   System.out.print("\nPago por hora $");
		   Pago_Horas = entrada.nextDouble();
		   System.out.print("\nHoras trabajados :");
		   Horas_Trabajadas = entrada.nextDouble();
		   System.out.print("\nDias trabajados por semana :");
		   Dias_Trabajados = entrada.nextInt();
		   Sueldo_Semanal = (Pago_Horas * Horas_Trabajadas) * Dias_Trabajados;
			
		   if((Sueldo_Semanal >= 0) && (Sueldo_Semanal <= 150)){
			   Sueldo_Final = Sueldo_Semanal * 0.95;
			   System.out.print("\nSueldo total [$"+Sueldo_Final+"]\n");
		   }//Fin if 
		   else if((Sueldo_Semanal > 150) && (Sueldo_Semanal < 300)){
			   Sueldo_Final = Sueldo_Semanal * 0.93;
			   System.out.print("\nSueldo total [$" + Sueldo_Final+ "]\n");
		   }//Fin else if 1
		   else if((Sueldo_Semanal >= 300) && (Sueldo_Semanal < 450)){
			  Sueldo_Final = Sueldo_Semanal * 0.91;
			  System.out.print("\nSueldo total [$" + Sueldo_Final + "]\n");
		   }//Fin else if 2
		}//Fin ciclo for
      }//Fin metodo main 
}//Fin clase suel_semanal


