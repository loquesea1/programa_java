/*Autor:  Raul   Hernandez   Lopez
 *correo: freeenergy1975@gmail.com
 *fecha:  26  de  enero  del  2021*/

import java.text.DecimalFormat;
import java.util.Scanner;

//1.- Ejecucion del metodo principal.
public class Metodos{//Clase principal.
    public static void main (String [] Neo){
	String respuesta;
		    
	//Creamos un objeto de nombre entrada de la clase Scanner.
	Scanner entrada = new Scanner(System.in);
	DecimalFormat limite = new DecimalFormat("#.##");

	System.out.print("\nPrograma con metodos que\ncalcula promedios\n"); 
	do{
	   /*sintaxis para crear un objeto:
	   claseQueContienElMetodoALLamar nombreDelObjeto = new contructorclase();*/
    	   Segunda objeto = new Segunda(); //objeto

    	   //llamada al metodo capturaCalificaciones de la clase Segunda.
           System.out.print("\nEl promedio de la materia es: " 
			   + limite.format(objeto.capturaCalificaciones()) 
			   + "\n___________________________________\n");
	   System.out.print("\nDeseas continuar? yes/no: ");
           respuesta = entrada.next();
	}while(respuesta.equalsIgnoreCase("yes"));
    }//Fin metodo principal
}//Fin clase Metodos


class Segunda {
   //3.- Ejecucion del metodo listaMaterias.
   private void listaMaterias(){
	//Creacion del arreglo que contiene el nombre de cada materia.   
 	String materias[] = {"\n1.-Programacion", "\n2.-Mate discretas", "\n3.-Calculo Dif\n"};
	byte x;
	//Impresion de elementos dentro del arreglo materias.
	for(x = 0; x < 3; x++){
	    System.out.printf( materias[x]);	
	}//for
   }//Fin metodo listaMaterias

   //Cuando se espera hacer una llamada a un metodo que se encuentra  en la misma clase
   //no es necesario crera un objeto 
   
   //2.-Ejecucion del metodo capturaCalificaciones.
   public double capturaCalificaciones(){
      Scanner entrada = new Scanner(System.in);
      //Declaracion de variables y definicion de valores de las unidades de las materias.
      byte selectMateria, programacion = 3,matematicas = 4, calculo = 5, x, tamanio = 0;
      float calificaciones[], sumaCalificaciones = 0;
      double promedio;
      String materiaSelect;

      listaMaterias();//llamando al metodo listaMaterias 
      //4.- Continuacion metodo capturaCalifcaciones
      System.out.print("\nDigita el numero de tu materia: ");
      selectMateria = entrada.nextByte();
      
      //Evaluacion de la seleccion del usuario, para determinar el valor de tamanio
      if (selectMateria == 1){
   	tamanio = programacion;
	materiaSelect = "Programacion";

      }
      else if (selectMateria == 2){
      	tamanio = matematicas;
	materiaSelect = "Matematicas discretas";
      }
      else if (selectMateria ==3){
      	tamanio = calculo;
	materiaSelect = "Calculo Diferencial";
      }
      else{
      	System.out.print("Opcion no valida");
	materiaSelect = "Calculo Calculo diferencial";
      }

      //Definicion del tamanio del arreglo
      calificaciones = new float [tamanio];
      
      //Captura de los datos de cada unidad.      
      for(x = 0; x < tamanio; x++){
	System.out.print("\nCalificacion Unidad " + (x+1) + ": " );
	calificaciones[x] = entrada.nextFloat();
	sumaCalificaciones += calificaciones[x];
      }//Fin for

      promedio = sumaCalificaciones / tamanio;
      
      System.out.print("___________________________________\nMateria :" + materiaSelect);

      //retorno del valor promedio de tipo double.
      return promedio;
   }//Fin metodo capturaCalificaciones
}//Fin clase segunda    	
