//Raul hernandez lopez(Berserker)
//freeenergy1975@gmail.com
//Lunes 17 de agosto del 2020

//Este programa determina el costo de una llamada telefonica con base a los minutos que dure

import java.util.Scanner;
//Inicio de la clase LLamadaTelefonica
public class LLamadaTelefonica{
	//Publicacion de metodo main
	public static void main(String args[]){
	Scanner entrada = new Scanner(System.in);
	//Declaracion de variables
	double minutos = 1.25;
	double costo;
	double tiempo;

	System.out.print("Quieres conocer el precio de tu llamada?\n\nLa tarifa de llamada telefonica por minuto es de:$" + minutos );
	//Recopilacion y procesamiento de datos
	System.out.print("\ncuántos minutos duró tu llamada? :");
	tiempo = entrada.nextFloat();
	costo = tiempo * minutos;
	//Impresion de resultados
	System.out.print("El costo de tu llamada es de : $" + costo);
	}//Fin del metod main
}//Fin de la clase LLamadaTelefonica
