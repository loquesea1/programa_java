//RAÚL_HERNÁNDEZ_LÓPEZ
//freeenergy@gmail.com
//20 de octubre del 2020

//Leer tres números enteros de un Digito y almacenarlos en una
//sola variable que contenga a esos tres dígitos Por ejemplo
//si A=5 y B=6 y C=2 entonces X=562

//publicacion de la clase Concatenar
import java.util.Scanner;
public class Concatenar{
    //inicio del metodo main
    public static void main(String args[]){
	//Declaracion de variables
	Scanner entrada = new Scanner(System.in);
	String cadena_1, cadena_2, cadena_3, cadena_4;
	//Recopila datos
	System.out.print("\nIngresa un número o caracter :");
	cadena_1 = entrada.next();
	System.out.print("\nIngresa otro número o caracter :");
        cadena_2 = entrada.next();
	System.out.print("\nIngresa otro número o caracter :");
        cadena_3 = entrada.next();
	//concatena los valores de las cadenas anteriores y las almacena en una sola "cadena_4"
	cadena_4 = cadena_1 + cadena_2 + cadena_3;
	//impresion de resultados
	System.out.print("\nValores concatenadios [" + cadena_4 + "]\n");
    }//fin del metodo main
}//fin de la clase Concatenar
