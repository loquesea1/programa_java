//Raul_Hernandez_Lopez
//freeenergy1975@gmail.com
//jueves 10 de septiembre del 2020

//Este programa te permite ordenar un numero N de elementos por medio
//del metodo de la burbuja

import java.util.Scanner;
//inicio de la clase
public class MetodoBurbuja{
	//inicio del metodo main
	public static void main(String args[]){
		//Declaracion de variables
		Scanner entrada= new Scanner (System.in);
		int x, y, z, w, tamano, New_ord;
		//deinicion de del tamano del arreglo
		System.out.print("Ingresa el numero de datos a ordenar :");
		tamano = entrada.nextInt();
		int orden[] = new int[tamano]; 
		//inicio ciclo for
		//captura de datos
		for(x = 0 ; x < tamano; x++){
			System.out.print("\nIngresa el valor a ordenar ");
			orden[x] = entrada.nextInt();
		}//fin ciclo for
		//inicio ciclo for2
		for(y = 0 ;y < tamano; y++){
			//inicio ciclo for 3
			for(z = 0 ; z < tamano - 1; z++){
				//inicio condicional if
				//cambio de posicion en caso de que la condicion se cumpla
				if(orden[z] > orden[z+1]){
					New_ord = orden[z+1];
					orden[z+1] = orden[z];
					orden[z] = New_ord;
				}//fin condicional if
			}//fin ciclo for 3 
		}//fin for 2
		System.out.print("Orden ASCENDENTE :");
		//inicio for 4
		for(w = 0; w < tamano; w++){
			//imprime el arreglo en funcion de x
			System.out.print("[" + orden[w]+ "]");
		}//fin for 4
	}//fin del metodo main
}//fin de la clase MetodoBurbuja
