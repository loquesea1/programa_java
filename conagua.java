//Raul_Hernandez_Lopez
//freeenergy1975@gmail.com
//09 de Octubre del 2020

//eterminar el pago que debe realizar una persona por el 
//total de metros cúbicos que consume de agua

import java.util.Scanner;
//Publicaion de la clase conagua
public class conagua{
	//Publicacion del metodo principal
	public static void main(String args[]){
		Scanner entrada = new Scanner(System.in);
		//Declaracion de variables
		double pago, costo_agua, metro_consumido;
		//Recopilacion de informacion
		System.out.print("\nIngresa el precio del agua por metro cubico :");
		costo_agua = entrada.nextDouble();
		System.out.print("Ingresa los metros cubicos de agua consumidos :");
		metro_consumido = entrada.nextDouble();
		//Define el monto a pagar
		pago = metro_consumido * costo_agua;
		//Impresion de resultados
		System.out.print("Tu pago es de [$" + pago + "]");
		
	}
}
