//Raul hernandez lopez(Berserker)
//freeenergy1975@gmail.com
//Miercoles 12 de agosto del 2020

//Este programa determina el total de dinero dentro de un monedero en el cual se conoce con
//certeza el valor de cada elemento

import java.util.Scanner;
//Inicio de nuestra clase monedero
public class monedero{
   //inicio de nuestro metod main//
   public static void main(String args[]){
	Scanner entrada = new Scanner(System.in);
	//Declaracion de variables y Creacion de un arreglo con los valores que contiene
	//el monedero//
	int[] dinero = {1, 5, 10, 20, 50};
	int suma = 0; 
	int valor = 0;
	int suma2 = 0;
	int valor2 = 0;
	int total = 0;

	System.out.print("Monedero\n");
		//recopilacion de datos haciendo uso de un ciclo for para calcular el 
		//total de dinero en monedas// 
		for(int x = 0; x <= 2; x++){
		System.out.print("\ncuantas monedas de: $ " + dinero[x] + " tiene? :");
		valor = entrada.nextInt();
		
		suma = suma + (dinero[x] * valor);
		
		
		} 
		//segunda recopilacion de datos haciendo uso del un ciclo for para calcular el 
		//total de dinero recabado en billetes//
		for(int y = 3; y <= 4; y++ ){
		System.out.print("\ncuantos billetes de $" + dinero[y] + " tiene? :");
		valor2= entrada.nextInt();
		suma2 = suma2 + (dinero[y] * valor2);
		}
	//suma de los valores obtenidos ndentro de los ciclos for//
	total= suma + suma2;
	//impresion de resultados//
	System.out.println("\nEl total de tu monedero es de : $"+ total + "\n" );
    }//Fin de nuestro metod main//

}//Fin de nuestra clase//
