/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Martes 11 de agosto del 2020*/

/*Este programa nos permite calcular la cantidad de dinero que tenemos a partir de una cantidad
 *  de billetes y monedas definidos por el usuario, el valor de cada moneda y billete tambien es
 *  definida por el usuario.*/

import java.util.Scanner;
//Creacion de la clase caja//
public class caja{
//Inicio de nuestro metodo main//
public static void main(String args[]){
	//Declaracion de variables//
	Scanner entrada = new Scanner(System.in);
	int variedadB;
	int variedadM;
	int suma = 0;
	int suma_M = 0;
	System.out.print("\ncuantos billetes de distinto valor tienes? : ");
	variedadB = entrada.nextInt();
	//ciclo for en el cual se calcula la cantidad de dinero recabado en billetes//
	for ( int x = 1; x <= variedadB; x++ ){
		int valor;
		int cantidad;
		System.out.print("cual es el valor del billete? : ");
		valor = entrada.nextInt();
	
		System.out.print("\ncuantos billetes con ese valor tienes? : ");
		cantidad = entrada.nextInt();
		
		suma = ( cantidad * valor );
		suma = (suma + suma);
	
			if(x == variedadB){
				System.out.println("la cantidad de dinero en los billetes es de : " +suma + "\n");
			}
		}
	System.out.print("cuantas monedas de distinto valor tienes ? : ");
	variedadM = entrada.nextInt();
	//segundo ciclo for en el cual se calcula la cantidad de dinero recabada en monedas//
	for (int y = 1; y <= variedadM; y++){
		int valorm;
		int cantidadm;
		System.out.print("cual es el valor de la moneda? : ");
		valorm = entrada.nextInt();

		System.out.print("cuantas monedas con ese valor tienes? : ");
		cantidadm = entrada.nextInt();
		
		suma_M = (cantidadm * valorm);
		suma_M = (suma_M + suma_M);
		
			if( y == variedadM){
				System.out.println("La cantidad de dinero en monedas es de : " + suma_M + "\n" );
			}
		}
	//suma de los valores recolectados en los dos ciclos for para posteriormente imprimir resultados//
	int total = (suma_M + suma);
	System.out.println("El total de la caja es de : $" + total );
	}//Fin de nuestro metodo main//
}//Fin de nuestra clase//
