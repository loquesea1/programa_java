/*RAÚL_HERNÁNDEZ_LÓPEZ
* freeenrgy1975@gmail.com
* 20 de octubre del 2020*/

/*Obtener la edad de una persona en meses, si se ingresa su edad en años y meses.
 *Ejemplo: Ingresado 3 años 4 meses debemostrar 40 meses.*/

import java.util.Scanner;
//publicacion de la clase
public class Edad{
   //publicacion del metod main
   public static void main (String args[]){
	//Declaracion de variables
	Scanner entrada = new Scanner(System.in);
	int Meses, Edad_Meses, Años;
	//Recopila datos
	System.out.print("\nCuantos años tienes ? :");
	Años = entrada.nextInt();
	System.out.print("\nIngresa los meses restantes :");
	Meses = entrada.nextInt();
	//calcula el numero de meses en los años y los suma con los meses restantes
	Edad_Meses = (Años * 12) + Meses;
	System.out.print("\nEdad en meses [" + Edad_Meses + "]\n");
   }//fin metodo main
}//Fin clase Edad
