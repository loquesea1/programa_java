//Raul hernandez lopez(Berserker)
//freeenergy1975@gmail.com
//Jueves 20 de agosto del 2020

//calcula el tiempo que tarda una persona en realizar un recorrido
import java.util.Scanner;
//Inicio de la Clase VelocidadConctante
public class VelocidadConstante{
	//Inicio del metodo main
	public static void main(String args[]){
	//entrada y procesamiento de datos
	Scanner entrada = new Scanner(System.in);
	float Velocidad, Distancia, Tiempo;

	System.out.print("CALCULADORA DE TIEMPO\n\nCual es la distancia en Km que recorriste? :");
	Distancia = entrada.nextFloat();

	System.out.print("\nA que velocidad 'Km/h'? :");
	Velocidad = entrada.nextFloat();
	
	Tiempo = (Distancia / Velocidad);
	//Impresion de resultados
	System.out.print("El tiempo que tardaste en Recorrer " + Distancia + "Km es de :" + Tiempo +"hrs" );
	
	}//Fin del metodo main

}//Fin de la clase VelocidadConctante
