//Raul Hernandez Lopez
//freeenergy1975@gmail.com
//16 de octubre del 2020

//En un hospital existen 3 áreas: Urgencias, Pediatría y Traumatología. El presupuesto anual del
//manera: Pediatría 42% y Traumatología 21%.

import java.util.Scanner;
//Incio clase Hospital
public class Hospital{
   public static void main(String args[]){
	//Declaracion de variables
	Scanner entrada = new Scanner(System.in);
	Double Presupuesto, Presupuesto_Pediatria, Presupuesto_Traumatologia, Presupuesto_Urgencias;
	//Recopila datos
	System.out.print("Ingresa el presupesto total :");
	Presupuesto = entrada.nextDouble();
	Presupuesto_Pediatria = Presupuesto * 0.42;
	Presupuesto_Traumatologia = Presupuesto * 0.12;
	Presupuesto_Urgencias = Presupuesto - (Presupuesto_Pediatria + Presupuesto_Traumatologia);
	//Impresion de resultados
	System.out.print("\nLa distribucion del presupuesto se divide de la siguiente manera :");	   
	System.out.print("\nPresupuesto pedriatia $" + Presupuesto_Pediatria);
	System.out.print("\nPresupuesto traumatologia $" + Presupuesto_Traumatologia);
	System.out.print("\nPresupuesto Urgencias $" + Presupuesto_Urgencias);
 	

   }
}
