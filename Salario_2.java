//RAÚL_HERNÁNDEZ_LÓPEZ
//freeenergy1975@gmail.com
//17 de octubre del 2020

//Calcular el nuevo salario de un empleado si se le descuenta el 20% de
//su salario actual.

import java.util.Scanner;
public class Salario_2{
    public static void main(String args[]){
	//Declaracion de variables
	Scanner entrada = new Scanner (System.in);
	double Salario_Actual, Descuento, Salario_Descuento;
	System.out.print("Salario Actual :");
	Salario_Actual = entrada.nextDouble();
	//Calcula el sueldo del empleado con el descuento del 20%
	Salario_Descuento = Salario_Actual * 0.80;
	Descuento = Salario_Actual * 0.20;
	//Impresion de resultados
	System.out.print( "\nEl descuento es igual a $" + Descuento
	+ "\nMonto a cobrar $" + Salario_Descuento );	
    }//Fin metodo main
}//Fin clase Salario_2
