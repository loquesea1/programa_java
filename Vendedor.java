//RAÚL_HERNÁNDEZ_LÓPEZ
//freeenrgy1975@gmail.com
//27 de Octubre del 2020

//Un vendedor recibe un sueldo base más un 10% extra por comisión de sus ventas
//el vendedor desea saber cuánto dinero obtendrá por concepto de comisiones por
//las tres ventas que realiza en el mes y el total que recibirá en el mes
//tomando en cuenta su sueldo base y comisiones.

import java.util.Scanner;
//Creacion de la clase Vendedor
public class Vendedor{
    //inicio del metodo main
    public static void main(String argsp[]){
	//Declaracion de variables
	Scanner entrada = new Scanner (System.in);
	double comision = 0, sueldoFijo, sueldoTotal, venta;
	int numeroVentas, posicion = 0, x;
	//Recopila datos
	System.out.print("\nIngresa el monto de tu sueldo actual $");
	sueldoFijo = entrada.nextDouble();
	System.out.print("\nIngresa el numero de ventas que realizaste :");
	numeroVentas = entrada.nextInt();
	//Determina el monto correspondiente por concepto de ventas
	for(x = 0; x < numeroVentas; x++){
	   posicion = posicion + 1;
	   System.out.print("\nIngresa el numero la venta [" + posicion + "] :"); 
	   venta = entrada.nextDouble();
           comision =+ (venta * 0.10);
	}//fin ciclo for
	//calcula el sueldo correspondiente
	sueldoTotal = comision + sueldoFijo;
	//Impresion de resultados
	System.out.println("El monto recabado por concepto de ventas es de [$" 
	+ comision + "]");
	System.out.println("Sueldo total [$" + sueldoTotal + "]");
    }//fin metodo main
}//fin clase Vendedor
