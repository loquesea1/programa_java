//Raul hernandez lopez(Berserker)
//freeenergy1975@gmail.com
//15 de octubre del 2020

//Una persona recibe un préstamo de $10,000.00 de un banco y 
//desea saber cuánto pagará de interés, si el banco le cobra 
//una tasa del 27% anual.

import java.text.DecimalFormat;
import java.util.Scanner;

//Inicio clase prestamo
public class prestamo {
    //inicio metodo main
    
    public static void main(String args[]){    
	//Declaracion de variables. 
	Scanner entrada = new Scanner(System.in);
	DecimalFormat limiteDecimal = new DecimalFormat("#.##");
	
	String fecha_prestamo_str,fecha_actual_str, prestamo_str;
	double monto, prestamo, tasa_interes = 0.27;
	int fecha_actual, fecha_prestamo, tiempo_transcurrido, x;
	
        System.out.print("\n[Calculo de interes anual]\n");
	
	//Entrada de datos
	System.out.print("Año del prestamo  :");
        fecha_prestamo_str = entrada.next();

        System.out.print("Año actual \t  :");
        fecha_actual_str = entrada.next();

        System.out.print("Cantidad prestada $");
        prestamo_str = entrada.next();

	   try{	
		//parseo de datos
		fecha_prestamo = Integer.parseInt(fecha_prestamo_str);
		fecha_actual = Integer.parseInt(fecha_actual_str);
		prestamo = Float.parseFloat(prestamo_str);

		tiempo_transcurrido = fecha_actual - fecha_prestamo;

                System.out.print("\n\nAño\tMonto a pagar\n");

                //determina el interes a pagar anualmente
        	for(x = 1; x <= tiempo_transcurrido; x++){

                    prestamo += (prestamo * tasa_interes);
                    //impresion de resultados

                    System.out.print((fecha_prestamo + x) + "\t$" 
				    + limiteDecimal.format(prestamo) + "\n");
        	}//Fin ciclo for

        	System.out.print("\n");

    	    }
	   catch(NumberFormatException error){
	    	System.out.print("\n\nUno de los valores ingresados no es de tipo numerico\n\n");
	    }//Fin catch1
    }//Fin metodo main
}//Fin clase prestamo


