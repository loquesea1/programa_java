//RAÚL   HERNÁNDEZ   LÓPEZ
//freeenergy1975@gmail.com
//16  de  octubre del 2020

/*Calcular el nuevo salario de un empleado si obtuvo un
 *incremento del 8% sobre su salario actual y un
 descuento de 2.5% por servicios.*/

import java.text.DecimalFormat;
import java.util.Scanner;

//inicio de la clase Nuevo_Salario
public class Nuevo_Salario{
    public static void main(String args[]){
	//Declaracion de variables    
	Scanner entrada = new Scanner (System.in);
	DecimalFormat limite = new DecimalFormat("#.##");
	double Salario_Actual, Descuento, Nuevo_Salario, Salario_Final;
	String salarioActual, eleccion;
	do{
	    //Recopilacion de datos
	    System.out.print("\n\nIngresa tu salario por mes $");
	    salarioActual = entrada.next();

	    try{
		//Procesamiento de datos
		Salario_Actual = Float.parseFloat(salarioActual);
		Nuevo_Salario = Salario_Actual + (Salario_Actual * 0.08);
		Descuento = Nuevo_Salario * 0.025;
		Salario_Final = Nuevo_Salario - Descuento;

		//Impresion de Resultados
		System.out.print("\nSe   te  otorgo  un incremento del 8%"
			+ "\nSin embargo  se te descontara el 2.5%"
			+ "\nMonto a  cobrar $" + limite.format(Salario_Final)
			+ "\n_____________________________________\n");
		
		//le brinda al usuario la opcion de volver a ejecutar el programa.
		System.out.print("\nDeseas continuar? yes/no :");
		eleccion = entrada.next();

	    }//Fin try

	    //Se ejecuta cuando el parseo de datos no es posible
	    catch(NumberFormatException exception){
		System.out.print("\nEl  valor  introducido como salario no es\n"
			         + "de  tipo  numerico, por favor  repita  el\n"
			         + "proceso, Reiniciando, espero por favor...\n"
				 + "________________________________________");

		eleccion = "yes";
	    }//Fin catch

	}while(eleccion.equalsIgnoreCase("yes"));

   }//Fin metodo principal
}//Fin clase Nuevo_salario
