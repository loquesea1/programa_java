//Raul_Hernandez_Lopez
//freeenergy1975@gmail.com
//Viernes 02 de octubre del 2020

//Calcula el precio de un boleto en funcion de los kilometros si se sabe que 
//el costo por kilometro es de 20.5

import java.util.Scanner;

//publicacion de la clase VIAJE_yadira
public class VIAJE_yadira{
	//publicacion del metodo main
	public static void main(String args[]){
		Scanner entrada = new Scanner(System.in);
		double kilometros = 20.5, costo = 0, distancia = 0;
			/*recopilacion de datos*/	
			System.out.print("\nA que distancia en kilomtros se encuentra tu destino? :");			   
			distancia = entrada.nextDouble();
			/*calcual el costo del boleto*/
			costo = (kilometros * distancia);
			/*impresion de resultados*/
			System.out.print("\nel costo de tu boleto es de [$" + costo + "]");
							
	}//Fin metodo main
}//Fin clase VIAJE_Yadira

