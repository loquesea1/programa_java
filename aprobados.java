//Raul hernandez lopez(Berserker)
//freeenergy1975@gmail.com
//Miercoles 17 de septiembre del 2020


//Este programa te permite almacenar una cantidad N de calificaciones y nombres
//de alumnos para despues clasificar los apribados de los no aprobados

import java.util.Scanner;
//inicio de la clase aprobados 
public class aprobados{
	//inicio del metod main
	public static void main(String args[]){
		//Declaracion de variables
		Scanner entrada = new Scanner(System.in);
		int aprob = 0, NoAprob = 0, num_alumn, contador = 0, x;
		System.out.print("\nIngresa el numero de alumnos a evaluar :");
		num_alumn = entrada.nextInt();
		//Declaracion de los arreglos que almacenaran los nombres y 
		//las calificaciones de los alumnos.
		String  nombres[] = new String[num_alumn];
		float calificaciones[] = new float[num_alumn];
		//recopilacion de nombres y calificaciones
		for(x = 0; x < num_alumn; x++){
			contador = contador + 1;
			System.out.print("\nNombre del alumno : ");
			nombres[x] = entrada.next();
			System.out.print("\nCalificacion :");
			calificaciones[x] = entrada.nextFloat();
			//proceso para definir si el alumno aprueba o no 
			if((calificaciones[x] >= 6) && (calificaciones[x] <= 10)){
				aprob = aprob + 1;
				System.out.print("\nEl alumno " + nombres[x] +" aprobo con una calificacion de [" + calificaciones[x] + "]");
			}	 
			else if ((calificaciones[x] >= 0) && (calificaciones[x] < 6)){
				NoAprob = NoAprob + 1; 
				System.out.print("\nEl alumno " + nombres[x] + " no aprobo.");
			}
		}	
		//impresion de resultados
		System.out.print("\nEL NUMERO DE APROBADO ES DE [" + aprob + "]");
		System.out.print("\nEL NUMERO DE REPROBADOS ES DE [" + NoAprob + "]" );
	}//Fin del metodo main
}//Fin de la clase aprobados
